//app.js
import { getToken, wxLogin} from '/service/requestNetwork.js';
import { getUserInfo,setUserInfo } from '/service/myService.js';

App({
  onShow:function (){
    // // 必须是在用户已经授权的情况下调用
    this.getAuthUserInfo();
  },
  onLaunch: function () {
    this.userLogin();
  },
  userLogin:function(){
    var _self = this;
    if (!getToken()) {
      this.login();
    } else {
      getUserInfo(function (data) {
        if (data) {
          _self.globalData.userInfo = data.data;
        }
      });
    }
  },
  getAuthUserInfo:function(){
    var _self = this;
    wx.getUserInfo({
      success(res) {
        const userInfo = res.userInfo
        const nickName = userInfo.nickName //昵称
        const avatarUrl = userInfo.avatarUrl//头像
        const gender = userInfo.gender // 性别 0：未知、1：男、2：女
        const province = userInfo.province// 省份
        const city = userInfo.city//城市
        const country = userInfo.country//国家
        console.log("授权成功保存用户信息");
        setUserInfo({ usrNickName: nickName, usrHeadImg: avatarUrl},function(d){
          if (d != null){
            _self.userLogin();//重新登录获取用户信息
          }
        });
      },
      fail(res) {
        setTimeout(function () {
          wx.navigateTo({
            url: '/pages/my/userAuthorize'
          });
        }, 600);
      }
    })
  },
  login:function(callback){
    // 登录
    var _self = this;
    wx.login({
      success: res => {
        wxLogin(res.code,function(data){
          if (data != null){
            _self.globalData.userInfo = data.data;
            callback == null || callback == undefined ? "" : callback(_self.globalData.userInfo);
          }else{
            wx.showToast({
              title: '登陆失败',
              icon: 'none',
              duration: 2000
            });  
          }
        });
      }
    })
  },
  globalData: {
    userInfo: null,
    getRoleStr: { "JC": "普通用户", "TG": "投顾", "DK": "大咖", "FXS": "分析师", "SSGS":"上市公司"}, 
  }
})