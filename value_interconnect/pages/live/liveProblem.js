import { getPageForNwsId } from '../../service/myService.js';
import { timestampFormat } from '../../utils/util.js';

class Problem{
  constructor(){
    this.isNetWork = false;
    this.dataObj = {};
    this.datas = [];
  }
  requestNetWork(isLoadMore,newId,callback){
    if (this.isNetWork) return;
    var obj = this.dataObj;
    if (isLoadMore && obj.hasNext != null && obj.hasNext != undefined && obj.hasNext == false) return;
    var pageNo = obj.pageNo;
    if (pageNo == null || pageNo == undefined || !isLoadMore) {
      pageNo = 1
    } else {
      pageNo = pageNo + 1;
    }
    var dataList = this.datas;
    if (!isLoadMore) pageNo = 1;
    var _self = this;
    this.isNetWork = true;

    getPageForNwsId(pageNo,newId,function(d){
      var data = [];
      for (var i = 0; i < d.data.result.length; i++) {
        var obj = d.data.result[i];
        obj.createdOn = timestampFormat(obj.createdOn);
        data.push(obj);
      }
      _self.isNetWork = false;
      if (d){
        _self.dataObj = d.data;
        _self.datas = isLoadMore ? _self.datas.concat(data) : data;
        callback(data);
      }
    });
  }
}

module.exports = new Problem();