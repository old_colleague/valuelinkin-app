import liveProblemUtil from './liveProblem.js'
import historyLiveUtil from './historyLive.js'
import { getNwsLiveContent } from '../../service/liveService.js';
import WxParse from '../../wxParse/wxParse.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type:0, //0直播预告，1已经开始直播了，2历史回顾，普通视频
    nwsId:0,
    loadingMsg: "",
    isShowLoading:true,
    problemData:[],
    historyData: [],
    object:{},
    showModalStatus:false,
    pwd:"",
    baseObj:{}
  },
  getPwd:function(e){
    var val = e.detail.value;
    this.setData({
      pwd: val
    });
  },
  confirm:function(){
    wx.navigateTo({
      url: '/pages/my/phoneChange/phoneChange'
    })

    // if (this.data.pwd == '927684'){
    //   this.setData({
    //     showModalStatus: false,
    //     object: this.data.baseObj
    //   });
    // }else{
    //   wx.showToast({
    //     title: "密码错误",
    //     icon: 'none',
    //     duration: 2000
    //   });
    // }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
    this.setData({
      type: options.type,
      nwsId:options.nwsId
    });
  },
  requestProblem:function(isLoadMore){
    var _self = this;
    liveProblemUtil.requestNetWork(isLoadMore,this.data.nwsId,function(d){
        _self.setData({
          problemData:d
        });
    });
  },
  requestHistory:function(){
    var _self = this;
    historyLiveUtil.requestNetWork(this.data.object.nwsStockCode, function (d) {
      _self.setData({
        historyData: d
      });
    });
  },
  requestNetWork:function(){
    var _self = this;
    _self.setData({
      loadingMsg: "正在加载...",
      isShowLoading: true,
    });
    getNwsLiveContent(this.data.nwsId,function(d){
      if (d == null){
        _self.setData({
          loadingMsg: "加载失败",
          isShowLoading: true,
        });
      }else{
        if (_self.data.type == 0) {
          d.data = _self.parseData(d.data);
        }
        _self.setData({
          isShowLoading: false,
          object: _self.data.showModalStatus?{}:d.data,
          baseObj: d.data
        });
        WxParse.wxParse('article', 'html', d.data.nwsContent, _self, 5);
        if (_self.data.type == 1 || _self.data.type == 0) {
          _self.requestProblem(false);
        }else if (_self.data.type == 2){
          _self.requestHistory();
        }
      }
    });
  },
  parseData: function (d) {
    var startTime = (new Date()).getTime() / 1000;
    var endTime = new Date(d.nwsPublishDate.replace(/-/g, "/")).getTime() / 1000;
    var time = parseInt(endTime - startTime);
    var nd = 24 * 60 * 60;
    var nh = 60 * 60;
    var nm = 60;
    d.sayTime = parseInt((time / nd) + "") + "天" + parseInt((time % nd / nh) + "") + "时" + parseInt((time % nd % nh / nm) + "") + "分"
    return d;
  },
  //刷新提问
  refreshProblemList:function(){
    this.requestProblem(false);
  },
  /**
 * 页面上拉触底事件的处理函数
 */
  onReachBottom: function () {
    if (this.data.type == 1) {
      this.requestProblem(true);
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.requestNetWork();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var app = getApp();
    var showModalStatus = false;
    if (app.globalData.userInfo.usrPhone == '' ||
      app.globalData.userInfo.usrPhone == undefined ||
      app.globalData.userInfo.usrPhone == null) {
      showModalStatus = true;
    }
    
    this.setData({
      showModalStatus: showModalStatus,
      object: !showModalStatus?this.data.baseObj:{}
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})