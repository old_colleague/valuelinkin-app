
import { getJchgPage, getSsgszbYgPage, getTzhdYgPage, getSsgszbZbPage, getTzhdZbPage, getNwsLiveContent } from '../../service/liveService.js';
import { timestampFormat } from '../../utils/util.js';
// 已报废
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headTitles: ["上市公司直连", "粒场活动", "精彩回顾"],
    selectIndex: 0,
    gszlDatas: [], //公司直连
    tzhdDatas: [],//投资互动轮播图
    jchgDatas: [],//精彩回顾
    tzhdItems: [],//投资互动轮播图
    gszlItems:[],//公司直连轮播图
    gszlDatasObj: {},
    tzhdDatasObj: {},
    jchgDatasObj: {},
    gszlIsNetWork: false,
    tzhdIsNetWork: false,
    jchgIsNetWork: false,
    loadingMsg: "",
    gszlIsShowLoading: false,
    tzhdIsShowLoading: false,
    jchgIsShowLoading: false
  },
  refresh: function () {
    this.requestNetwork(false);
  },
  loadMore: function () {
    this.requestNetwork(true);
  },
  requestGszlzb(){
    var _self = this;
    getSsgszbZbPage(function(d){
      if (d){
        _self.setData({
          gszlItems:d.data.result
        });
      }
    });
  },
  requestTzhdzb(){
    var _self = this;
    getTzhdZbPage(function (d) {
      if (d) {
        _self.setData({
          tzhdItems: d.data.result
        });
      }
    });
  },
  requestNetwork: function (isLoadMore) {
    if (this.data.selectIndex == 0 && this.data.gszlIsNetWork) return;
    if (this.data.selectIndex == 1 && this.data.tzhdIsNetWork) return;
    if (this.data.selectIndex == 2 && this.data.jchgIsNetWork) return;

    var obj = this.data.selectIndex == 0 ? this.data.gszlDatasObj : this.data.selectIndex == 1 ? this.data.tzhdDatasObj : this.data.jchgDatasObj;
    if (isLoadMore && obj.hasNext != null && obj.hasNext != undefined && obj.hasNext == false) return;
    var pageNo = obj.pageNo;
    if (pageNo == null || pageNo == undefined || !isLoadMore) {
      pageNo = 1
    } else {
      pageNo = pageNo + 1;
    }
    var dataList = this.data.selectIndex == 0 ? this.data.gszlDatas : this.data.selectIndex == 1 ? this.data.tzhdDatas : this.data.jchgDatas;
    if (!isLoadMore) pageNo = 1;
    if (dataList.length == 0) {
      if (this.data.selectIndex == 0) {
        this.setData({
          gszlIsShowLoading: true
        });
      } else if (this.data.selectIndex == 1) {
        this.setData({
          tzhdIsShowLoading: true
        });
      } else {
        this.setData({
          jchgIsShowLoading: true
        });
      }
      this.setData({
        loadingMsg: "正在加载..."
      });
    }
    this.data.selectIndex == 0 ? this.data.gszlIsNetWork = true :
      this.data.selectIndex == 1 ? this.data.tzhdIsNetWork = true : this.data.jchgIsNetWork = true;
    var _self = this;

    console.log((isLoadMore ? "加载更多:" : "刷新:") + pageNo);
    if (this.data.selectIndex == 0) {
      this.requestGszlzb();
      getSsgszbYgPage(pageNo, function (d) {
        _self.data.gszlIsNetWork = false;
        if (d) {
          if (d.data.result == 0 && _self.data.gszlDatas.length == 0) {
            _self.setData({
              gszlIsShowLoading: false
            });
          } else {
            _self.setData({
              gszlIsShowLoading: false,
              loadingMsg: "",
              gszlDatas: isLoadMore ? _self.data.gszlDatas.concat(_self.parseData(d.data.result)) : _self.parseData(d.data.result),
              gszlDatasObj: d.data
            });
          }
        } else {
          _self.setData({ gszlIsShowLoading: false });
        }
        console.log(_self.data);
      });
    } else if (this.data.selectIndex == 1) {
      this.requestTzhdzb();
      getTzhdYgPage(pageNo, function (d) {
        _self.data.tzhdIsNetWork = false;
        if (d) {
          if (d.data.result == 0 && _self.data.tzhdDatas.length == 0) {
            _self.setData({
              tzhdIsShowLoading: false
            });
          } else {
            _self.setData({
              tzhdIsShowLoading: false,
              loadingMsg: "",
              tzhdDatas: isLoadMore ? _self.data.tzhdDatas.concat(_self.parseData(d.data.result)) : _self.parseData(d.data.result),
              tzhdDatasObj: d.data
            });
          }
        } else {
          _self.setData({ tzhdIsShowLoading: false });
        }
      });
    } else if (this.data.selectIndex == 2) {
      getJchgPage(pageNo, function (d) {
        _self.data.jchgIsNetWork = false;
        if (d) {
          if (d.data.result == 0 && _self.data.jchgDatas.length == 0) {
            _self.setData({
              jchgIsShowLoading: true,
              loadingMsg: "暂无数据"
            });
          } else {
            var jchgList = _self.parseJchg(d.data.result);
            _self.setData({
              jchgIsShowLoading: false,
              loadingMsg: "",
              jchgDatas: isLoadMore ? _self.data.jchgDatas.concat(jchgList) : jchgList,
              jchgDatasObj: d.data
            });
          }
        } else {
          _self.setData({ jchgIsShowLoading: false });
        }
      });
    }
  },
  parseJchg:function(data){
    var list = [];
    data.forEach(function(item){
      var data = {};
      data.opnTitle = item.nwsTitle;
      data.opnThumbnailUrl = item.nwsThumbnailUrl;
      data.opnBrief = item.nwsBrief;
      data.opnType = 1;
      data.opnViewCount = item.nwsViewCount;
      data.opnPublishDate = timestampFormat(item.nwsPublishDate);
      data.opnId = item.nwsId;

      list.push(data);
    });
    return list;
  },
  parseData:function(d){
    var data = [];
    d.forEach(item => {
      var startTime = (new Date()).getTime() / 1000;
      var endTime = new Date(item.nwsPublishDate.replace(/-/g, "/")).getTime() / 1000;
      var time = parseInt(endTime - startTime);
      var nd = 24 * 60 * 60;
      var nh = 60 * 60;
      var nm = 60;
      item.sayTime = parseInt((time / nd) + "") + "天" + parseInt((time % nd / nh) + "") + "时" + parseInt((time % nd % nh / nm) + "") + "分"
      data.push(item);
    });
    return data;
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      selectIndex:options.index
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.requestNetwork(false);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /***
 * 顶部菜单左右滑动
 */
  changeItem: function (e) {
    console.log(e.detail);
    this.setData({
      selectIndex: e.detail
    });
  },
  /***
   * 下面页面左右滑动
   */
  swiperChange: function (e) {
    this.data.selectIndex = e.detail.current;
    this.selectComponent("#scrollMenu").setSeIndex(this.data.selectIndex);
    this.requestNetwork(false);

  },
})