import { getRelationVideo } from '../../service/liveService.js';
import { timestampFormat } from '../../utils/util.js';

class RelationVideo {
  constructor() {
    this.isNetWork = false;
    this.datas = [];
  }
  requestNetWork(stkCode, callback) {
    if (this.isNetWork) return;
    this.isNetWork = true;
    var _self = this;
    getRelationVideo(stkCode, function (d) {
      _self.isNetWork = false;
      if (d) {
        var data = [];
        for (var i = 0; i < d.data.length; i++) {
          var obj = d.data[i];
          obj.createdOn = timestampFormat(obj.createdOn);
          data.push(obj);
        }
        _self.datas = data;
        callback(data);
      }
    });
  }
}

module.exports = new RelationVideo();