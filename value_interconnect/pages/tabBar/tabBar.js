// pages/tabBar/tabBar.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    items: [
      {
        "iconPath": "/img/tabbar/home_n.png",
        "selectedIconPath": "/img/tabbar/home_h.png",
        "text": "首页",
        "selectColor": "#1772FC"
      },
      {
        "iconPath": "/img/tabbar/live_n.png",
        "selectedIconPath": "/img/tabbar/live_h.png",
        "text": "直播",
        "selectColor": "#1772FC"
      },
      {
        "iconPath": "/img/tabbar/com_n.png",
        "selectedIconPath": "/img/tabbar/com_h.png",
        "text": "上市公司",
        "selectColor": "#1772FC"
      },
      {
        "iconPath": "/img/tabbar/gz_n.png",
        "selectedIconPath": "/img/tabbar/gz_h.png",
        "text": "观点",
        "selectColor": "#1772FC"
      },
      {
        "iconPath": "/img/tabbar/my_n.png",
        "selectedIconPath": "/img/tabbar/my_h.png",
        "text": "我的",
        "selectColor": "#1772FC"
      },            
    ]
  },
  changeItem(event){
    this.changeTab(event.currentTarget.dataset.index);
  },
  changeTab(index){
    if ((typeof index) != 'number'){
      index = index.detail;
    }
    this.setData({
      currentTab: index
    });
    wx.setNavigationBarTitle({
      title: index == 0 ? "古东管家" : this.data.items[index].text
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})

// "tabBar": {
//   "borderStyle": "black",
//   "selectedColor": "#ffba00",
//   "color": "#cccccc",
//   "list": [
//     {
//       "pagePath": "pages/home/home",
//       "text": "首页",
//       "iconPath": "/img/stock.png",
//       "selectedIconPath": "/img/stock_h.png"
//     },
//     {
//       "pagePath": "pages/live/live",
//       "text": "互连",
//       "iconPath": "/img/conect.png",
//       "selectedIconPath": "/img/conect_h.png"
//     },
//     {
//       "pagePath": "pages/viewpoint/viewpoint",
//       "text": "观点",
//       "iconPath": "/img/viewpoint.png",
//       "selectedIconPath": "/img/viewpoint_h.png"
//     },
//     {
//       "pagePath": "pages/my/my",
//       "text": "我的",
//       "iconPath": "/img/my.png",
//       "selectedIconPath": "/img/my_h.png"
//     }
//   ]
// }