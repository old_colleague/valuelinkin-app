import { getStockMg } from '../../service/homeService.js';
import WxParse from '../../wxParse/wxParse.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    selectIndex: 0,
    dgjisShowLoading:false,
    loadingMsg:"",
    dgj:[]
  },
  stkCode:null,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.stkCode = options.stkCode;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getComStockMg();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getComStockMg: function () {
    if (this.data.dgjisShowLoading) return;
    if (this.data.dgj.length == 0) {
      this.setData({
        loadingMsg: "正在加载...",
        dgjisShowLoading: true
      });
    }
    var _self = this;
    getStockMg(this.stkCode, function (d) {
      if (d) {
        if (d.data.length == 0) {
          _self.setData({
            loadingMsg: "暂无数据",
            dgjisShowLoading: true
          });
        } else { 
          WxParse.wxParse('reply', 'html', d.data[0].mgrResume, _self);

          for (let i = 0; i < d.data.length; i++) {
            WxParse.wxParse('reply' + i, 'html', d.data[i].mgrResume, _self);
            if (i === d.data.length - 1) {
              WxParse.wxParseTemArray("replyTemArray", 'reply', d.data.length, _self)
            }
          }
          _self.setData({ dgjisShowLoading: false, dgj: d.data });
        }
      } else {
        _self.setData({ dgjisShowLoading: false });
      }
    });
  },
}) 