import { queryStockGroupPage, getNewsflushPage, getGgxxPage } from '../../service/homeService.js';
import { timestampFormat } from '../../utils/util.js';
class HomeData {
  constructor() {
    this.isNetWork = false;
    this.datas = [];
    this.pageObj = {};
    this.showMsgFunction = null;
  }
  requestSsgs(callback){
    if (this.isNetWork) return;
    this.isNetWork = true;
    var _self = this;
    if (this.datas.length == 0) {
      this.showMsgFunction({
        isShow: true,
        loaddingMessage: "正在加载..."
      });
    }
    var _self = this;
    queryStockGroupPage(function (d) {
      _self.isNetWork  = false;
      if (d == null && _self.datas.length == 0) {
        _self.showMsgFunction({
          isShow: true,
          loaddingMessage: "获取上市公司数据失败",
        });
      } else {
        _self.datas = d.data;
        callback(d.data);
      }
    });
  }
  gethasKxData(publishDate){
    for (var i = 0; i < this.datas.length; i++) {
      var node = this.datas[i];
      if (node.time == publishDate) {
        return node;
      }
    }
    return null;
  }
  requestKx(isLoadMore,callback){
    if (this.isNetWork) return;
    var _self = this;
    var obj = this.pageObj;
    if (isLoadMore && obj.hasNext != null && obj.hasNext != undefined && obj.hasNext == false) return;
    var pageNo = obj.pageNo;
    if (pageNo == null || pageNo == undefined || !isLoadMore) {
      pageNo = 1
    } else {
      pageNo = pageNo + 1;
    }
    var dataList = this.datas;
    if (!isLoadMore) pageNo = 1;
    if (dataList.length == 0) {
      _self.showMsgFunction({
        isLoadding: true,
        loadingMsg: "正在加载..."
      });
    }
    console.log((isLoadMore ? "加载更多:" : "刷新:") + pageNo);
    this.isNetwork = true;
    getNewsflushPage(pageNo, function (d) {
      _self.isNetwork = false;
      if (d == null && _self.datas.length == 0) {
        _self.showMsgFunction({
          isKxShow: true,
          loaddingMessage: "获取数据失败"
        });
        return;
      }
      _self.pageObj = d.data;
      var list = d.data.result;
      if (!isLoadMore) {
        _self.datas = [];
      }
      //根据 时间按照天归类。
      for (var i = 0; i < list.length; i++) {
        var object = list[i];
        var date = object.publishDate.split("-");
        date = date[1] + "-" + date[2];
        var subDatas = _self.gethasKxData(date);
        if (subDatas == null) { //说明日期是新的，则新建一个日期分类数组放进去。
          subDatas = {};
          subDatas.time = date;
          subDatas.list = [object];
          _self.datas.push(subDatas);
        } else {
          subDatas.list.push(object);
        }
      }
      callback(_self.datas);
    });
  }
  requestGg(isLoadMore, callback){
    if (this.isNetWork) return;
    var _self = this;
    var obj = this.pageObj;
    if (isLoadMore && obj.hasNext != null && obj.hasNext != undefined && obj.hasNext == false) return;
    var pageNo = obj.pageNo;
    if (pageNo == null || pageNo == undefined || !isLoadMore) {
      pageNo = 1
    } else {
      pageNo = pageNo + 1;
    }
    var dataList = this.datas;
    if (!isLoadMore) pageNo = 1;
    if (dataList.length == 0) {
      _self.showMsgFunction({
        isLoadding: true,
        loadingMsg: "正在加载..."
      });
    }
    console.log((isLoadMore ? "加载更多:" : "刷新:") + pageNo);
    this.isNetwork = true;
    getGgxxPage(pageNo, function (d) {
      _self.isNetwork = false;
      if (d == null && _self.datas.length == 0) {
        _self.showMsgFunction({
          isKxShow: true,
          loaddingMessage: "获取数据失败"
        });
        return;
      }
      _self.pageObj = d.data;
      var list = _self.parseDateTime(d.data.result);
      if (!isLoadMore) {
        _self.datas = [];
      }
      _self.datas = isLoadMore ? _self.datas.concat(list) : list;
      callback(_self.datas);
    });
  }
  parseDateTime(d){
    var data = [];
    for (var i = 0; i < d.length; i++) {
      var obj = d[i];
      obj.opnPublishDate = timestampFormat(obj.opnPublishDate);
      data.push(obj);
    }
    return data;
  }

}

module.exports = HomeData;