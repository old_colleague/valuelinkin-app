import { getPageByStock, getNwsContent, getStockJPCL, getStockReport, getStockMg, getStockShareholding,deleteStockWacth, addStockWacth  } from '../../service/homeService.js';
import { toDecimal2 } from '../../utils/util.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headTitles: ["快讯公告", "金牌策略", "研究报告", "股本结构", "董高监"],
    kxggObj:{},
    kxggisShowLoading:false,
    kxgg:[],
    jpclObj: {},
    jpclisShowLoading: false,
    jpcl:[],
    yjbgObj: {},
    yjbgisShowLoading: false,
    yjbg:[],
    gbjgisShowLoading: false,
    gbjg:[],
    dgj:[],
    dgjisShowLoading: false,
    contentObj:null,
    selectIndex: 0,
    pieHeight:0,
    loadingMsg: "",
    colors: ["#ffba00", "#000000", "#242424", "#5d5d5d", "#b9b9b9", "#929292","#dcdcdc"]
  },
  stkCode:null,
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.stkCode = options.stkCode;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    const query = wx.createSelectorQuery()
    query.select('#mypie').boundingClientRect()
    var _self = this;
    query.exec(function (res) {
      _self.data.pieHeight = res[0].height;
    })
  },
  getNumber: function(num){
    return 10;
  },
  getContentData:function(){
    var _self = this;
    getNwsContent(this.stkCode,function(d){
        if (d){
          _self.setData({
            contentObj: d.data
          });  
        }
    });
  },
  getComStockMg: function(){
    if (this.data.dgjisShowLoading) return;
    if (this.data.dgj.length == 0) {
      this.setData({
        loadingMsg: "正在加载...",
        dgjisShowLoading: true
      });
    }
    var _self = this;
    getStockMg(this.stkCode, function (d) {
      if (d) {
        if (d.data.length == 0) {
          _self.setData({
            loadingMsg: "暂无数据",
            dgjisShowLoading: true
          });
        } else {
          _self.setData({ dgjisShowLoading: false, dgj: d.data });
        }
      } else {
        _self.setData({ dgjisShowLoading: false });
      }
    });
  },
  gbjgLoadMore: function (){
    if (this.data.gbjgisShowLoading) return;
    if (this.data.gbjg.length == 0){
      this.setData({
        loadingMsg: "正在加载...",
        gbjgisShowLoading:true
      });      
    }
    var _self = this;
    getStockShareholding(this.stkCode,function(d){
      if (d){
          if (d.data.length == 0){
            _self.setData({
              loadingMsg: "暂无数据",
              gbjgisShowLoading: true
            }); 
          }else{
            _self.setData({ gbjgisShowLoading: false, gbjg:d.data });     
            setTimeout(function () { _self.drawPie();},500);
          }
      }else{
        _self.setData({gbjgisShowLoading: false}); 
      }
    });
  },
  jpclLoadMore: function (e){
    this.loadPage(e);
  },
  kxggLoadMore: function (e){
    this.loadPage(e);
  },
  yjbgLoadMore:function (e){
    this.loadPage(e);
  },
  loadPage:function(e){
    var isLoadMore = false;
    if (e) {
      isLoadMore = true;
    }
    var obj = this.data.selectIndex == 0 ? this.data.kxggObj : this.data.selectIndex == 1 ? this.data.jpclObj : this.data.yjbgObj;
    if (obj.hasNext != null && obj.hasNext != undefined && obj.hasNext == false) return;
    var pageNo = obj.pageNo;
    if (pageNo == null || pageNo == undefined || !isLoadMore) {
      pageNo = 1
    } else {
      pageNo = pageNo + 1;
    }
    var dataList = this.data.selectIndex == 0 ? this.data.kxgg : this.data.selectIndex == 1 ? this.data.jpcl : this.data.yjbg;
    if (!isLoadMore) pageNo = 1;
    if (dataList.length == 0) {
      if (this.data.selectIndex == 0){
        this.setData({
          kxggisShowLoading: true
        });        
      } else if (this.data.selectIndex == 1) {
        this.setData({
          jpclisShowLoading:true
        });
      }else{       
        this.setData({         
          yjbgisShowLoading: true
        });
      }
      this.setData({
        loadingMsg: "正在加载..."
      });
    }
    var _self = this;
    if (this.data.selectIndex == 0){
      getPageByStock(pageNo, this.stkCode, function (d) {
        if (d) {
          if (d.data.result == 0 && _self.data.kxgg.length == 0) {
            _self.setData({
              kxggisShowLoading: true,
              loadingMsg: "暂无数据"
            });
          } else {
            _self.setData({
              kxggisShowLoading: false,
              loadingMsg: "",
              kxgg: isLoadMore ? _self.data.kxgg.concat(d.data.result) : d.data.result,
              kxggObj: d.data
            });
          }
        }else{
          _self.setData({ kxggisShowLoading: false });
        }
      });
    }else if (this.data.selectIndex == 1){
      getStockJPCL(pageNo, this.stkCode, function (d) {
        if (d) {
          if (d.data.result == 0 && _self.data.jpcl.length == 0) {
            _self.setData({
              jpclisShowLoading: true,
              loadingMsg: "暂无数据"
            });
          } else {
            _self.setData({
              jpclisShowLoading: false,
              loadingMsg: "",
              jpcl: isLoadMore ? _self.data.jpcl.concat(d.data.result) : d.data.result,
              jpclObj: d.data
            });
          }
        } else {
          _self.setData({ jpclisShowLoading: false });
        }
      });      
    } else if (this.data.selectIndex == 2) {
      getStockReport(pageNo, this.stkCode, function (d) {
        if (d) {
          if (d.data.result == 0 && _self.data.yjbg.length == 0) {
            _self.setData({
              yjbgisShowLoading: true,
              loadingMsg: "暂无数据"
            });
          } else {
            _self.setData({
              yjbgisShowLoading: false,
              loadingMsg: "",
              yjbg: isLoadMore ? _self.data.yjbg.concat(d.data.result) : d.data.result,
              yjbgObj: d.data
            });
          }
        }else{
          _self.setData({ yjbgisShowLoading: false });
        }
      });
    }
  },
  /***
    * 订阅
    */
  subscribeltem: function () {
    var _self = this;
    var item = this.data.contentObj.vlnStockEntity;
    if (item.watchFlag == "Y") { //表示已订阅，再次点击就取消订阅
      deleteStockWacth(item.stkCode, function (d) {
        if (d) {
          wx.showToast({
            title: '取消成功',
            icon: 'none',
            duration: 2000
          });
          _self.data.contentObj.vlnStockEntity.watchFlag = "N";
          _self.setData({
            contentObj: _self.data.contentObj
          });
        }
      });
    } else { //订阅
      addStockWacth(item.stkCode, function (d) {
        if (d) {
          wx.showToast({
            title: '订阅成功',
            icon: 'none',
            duration: 2000
          });
          _self.data.contentObj.vlnStockEntity.watchFlag = "Y";
          _self.setData({
            contentObj: _self.data.contentObj
          });
        }
      });
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getContentData();
    this.kxggLoadMore(null);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /***
 * 顶部菜单左右滑动
 */
  changeItem: function (e) {
    this.setData({
      selectIndex: e.detail
    });
  },
  /***
   * 下面页面左右滑动
   */
  swiperChange: function (e) {
    this.data.selectIndex = e.detail.current;
    this.selectComponent("#scrollMenu").setSeIndex(this.data.selectIndex);
    if (this.data.selectIndex == 0 && this.data.kxgg.length == 0) {
      this.kxggLoadMore(null);
    } else if (this.data.selectIndex == 1 && this.data.jpcl.length == 0) {
      this.jpclLoadMore(null);
    } else if (this.data.selectIndex == 2 && this.data.yjbg.length == 0) {
      this.yjbgLoadMore(null);
    } else if (this.data.selectIndex == 3 && this.data.gbjg.length == 0) {
      this.gbjgLoadMore();
    } else if (this.data.selectIndex == 4 && this.data.dgj.length == 0) {
      this.getComStockMg();
    } 
  },
  drawPie:function(){
    var data = [];
    for (var i =0;i< this.data.gbjg.length;i++){
      data.push({ "value": this.data.gbjg[i].sshWeight * 100.00,"color":this.data.colors[i]});
    }

    data = this.parsePieData(data);
    //使用wx.createContext获取绘图上下文context
    var context = wx.createContext();   
    //    定义圆心坐标
    var point = { x: this.data.pieHeight / 2, y: this.data.pieHeight/2 };
    //    定义半径大小
    var radius = this.data.pieHeight / 2;
    /*    循环遍历所有的pie */
    for (var i = 0; i < data.length; i++) {
      context.beginPath();
      //    	起点弧度
      var start = 0;
      if (i > 0) {
        // 计算开始弧度是前几项的总和，即从之前的基础的上继续作画
        for (var j = 0; j < i; j++) {
          start += data[j].value * 2 * Math.PI;
        }
      }
      console.log("i:" + i);
      console.log("start:" + start);
      console.log("data[i].value:" + data[i].value);
      //      1.先做第一个pie
      //   	2.画一条弧，并填充成三角饼pie，前2个参数确定圆心，第3参数为半径，第4参数起始旋转弧度数，第5参数本次扫过的弧度数，第6个参数为时针方向-false为顺时针
      context.arc(point.x, point.y, radius, start, start+data[i].value * 2 * Math.PI, false);
      //      3.连线回圆心
      context.lineTo(point.x, point.y);
      //      4.填充样式
      context.setFillStyle(data[i].color);
      //      5.填充动作
      context.fill();
      context.closePath();
    }
    //调用wx.drawCanvas，通过canvasId指定在哪张画布上绘制，通过actions指定绘制行为
    wx.drawCanvas({
      //指定canvasId,canvas 组件的唯一标识符
      canvasId: 'mypie',
      actions: context.getActions()
    });
  },
  parsePieData: function(data){
    var parseData = [];
    var sum = 0;
    for (var i =0;i< data.length;i++){
      sum = data[i].value + sum;
    }
    for (var i = 0; i < data.length; i++) {
      var obj  = data[i];
      parseData.push({ "value": obj.value / sum,"color":obj.color});
    }
    return parseData;
  }
})