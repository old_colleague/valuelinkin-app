// pages/home/home.js
import { queryStockGroupPage, getNewsflushPage } from '../../service/homeService.js';
import HomeData  from './homeData.js'

// 已报废
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headTitles: ["公告秀秀","快讯"],
    selectIndex: 0,
    ssgs:[],
    kx: [],
    ggxx:[],
    isShow:false,
    isKxShow: false,
    isGgShow:false,
    loaddingMessage:"正在加载...",
    ssgsObj: null,
    kxObj:null,
    ggxxObj:null
  },
   
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.ssgsObj = new HomeData();
    this.data.ssgsObj.showMsgFunction = this.showMsg;
    this.data.kxObj = new HomeData();
    this.data.kxObj.showMsgFunction = this.showMsg;
    this.data.ggxxObj = new HomeData();
    this.data.ggxxObj.showMsgFunction = this.showMsg;    
    this.setData({
      selectIndex: options.index
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.requestSsgsNetWork();
    if (this.data.selectIndex == 0){
      this.getGgxxData(false);
    }else if (this.data.selectIndex == 1){
      this.getNewsflush(false);
    }else{
      
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  showMsg: function(option){
    var obj = {};
    if (this.data.selectIndex == 0){
      // obj.isShow = option.isShow;
      obj.isGgShow = option.isShow;
    } else if (this.data.selectIndex == 2){
      
    }else if (this.data.selectIndex == 1){
      obj.isKxShow = option.isShow;
    }
    obj.loaddingMessage = option.loaddingMessage;
    this.setData(obj);
  },
  refresh: function(){
    console.log("刷新数据");
    if (this.data.selectIndex == 0) {
      // this.requestSsgsNetWork();
      this.getGgxxData(false);
    } else if (this.data.selectIndex == 2){
      
    }else if (this.data.selectIndex == 1){
      this.getNewsflush(false);
    }
  },
  loadMore:function(){
    if (this.data.selectIndex == 1){
      this.getNewsflush(true);
    }else if (this.data.selectIndex == 0){
      this.getGgxxData(true);
    }
  },
  getGgxxData: function (isLoadMore){
    var _self = this;
    this.data.ssgsObj.requestGg(isLoadMore, function (d) {
      _self.setData({
        ggxx: d,
        isGgShow: false
      });
    });
  },
  /**
   * 获取快讯列表
   */
  getNewsflush:function(isLoadMore){
    var _self = this;
    this.data.ssgsObj.requestKx(isLoadMore,function (d) {
      _self.setData({
        kx: d,
        isKxShow: false
      });
    });
  },

  /**
   * 获取首页上市公司
   */
  requestSsgsNetWork:function(){
    var _self = this;
    this.data.ssgsObj.requestSsgs(function(d){
        _self.setData({
          ssgs: d,
          isShow: false
        });    
    });
  },
  /***
  * 顶部菜单左右滑动
  */
  changeItem: function (e) {
    console.log(e.detail);
    this.setData({
      selectIndex: e.detail
    });
    // this.refresh();
  },
  /***
   * 下面页面左右滑动
   */
  swiperChange: function (e) {
    this.data.selectIndex = e.detail.current;
    this.selectComponent("#scrollMenu").setSeIndex(this.data.selectIndex);
    this.refresh();
  },
})