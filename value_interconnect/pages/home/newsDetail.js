import { getNewsflushDetail, getStockReportDesc } from '../../service/homeService.js';
import WxParse from '../../wxParse/wxParse.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    loaddingMessage:"正在加载...",
    isShow:false,
    object:null
  },
  newsId:null,
  type: 0, //快讯  1股票快讯公告，nwsId  2 金牌策略，opnId 3研究报告 id

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.newsId = options.nwsId;
    this.type = options.type;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.object == null){
      this.setData({
        loaddingMessage: "正在加载...",
        isShow: true
      });
    }
    var _self = this;
    if (this.type == 3){
      getStockReportDesc(this.newsId,function(d){
        if (d == null && _self.data.object == null) {
          _self.setData({
            loaddingMessage: "加载失败",
            isShow: true
          });
        } else {
          var data = {};
          data.nwsTitle = d.data.rptTitle;
          data.nwsSource = d.data.rptSource;      
          data.nwsPublishDate = d.data.rptPublishDate;     
          data.nwsBrief = d.data.rptContent;                    
          
          _self.setData({
            object: data,
            isShow: false
          });
          WxParse.wxParse('article', 'html', data.nwsBrief, _self, 5);
        }
      });

    }else{
      getNewsflushDetail(this.newsId, function (d) {
        if (d == null && _self.data.object == null) {
          _self.setData({
            loaddingMessage: "加载失败",
            isShow: true
          });
        } else {
          _self.setData({
            object: d.data,
            isShow: false
          });
          WxParse.wxParse('article', 'html', d.data.nwsBrief, _self, 5);
        }
      });
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return { path: '/pages/home/newsDetail?nwsId=' + this.newsId};
  }
})