import { getOpnContent } from '../../service/homeService.js';
import WxParse from '../../wxParse/wxParse.js';
import { timestampFormat } from '../../utils/util.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loaddingMessage: "正在加载...",
    isShow: false,
    object: null
  },
  opnId:null,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.opnId = options.opnId;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.object == null) {
      this.setData({
        loaddingMessage: "正在加载...",
        isShow: true
      });
    }
    var _self = this;
    getOpnContent(this.opnId, function (d) {
      if (d == null && _self.data.object == null) {
        _self.setData({
          loaddingMessage: "加载失败",
          isShow: true
        });
      } else {
        d.data.opnPublishDate = timestampFormat(d.data.opnPublishDate);
        _self.setData({
          object: d.data,
          isShow: false
        });

        WxParse.wxParse('article', 'html', d.data.opnContent, _self, 5);
        // 
      }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})