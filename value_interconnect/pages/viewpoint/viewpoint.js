import { getDkksPage, getJpclPage, getMsjdPage } from '../../service/homeService.js';
import { timestampFormat } from '../../utils/util.js';

// 已报废
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headTitles: ["大咖看市", "名师解读","金牌策略"],
    selectIndex:0,
    dkksDatasObj:{},
    msjdDatasObj: {},
    jpclDatasObj: {},
    dkksIsNetWork:false,
    msjdIsNetWork: false,
    jpclIsNetWork: false,
    dkksDatas: [],
    msjdDatas: [],
    jpclDatas: [],
    loadingMsg:"",
    dkksIsShowLoading: false,
    msjdIsShowLoading: false,
    jpclIsShowLoading: false,
    userRole:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      selectIndex: options.index
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.requestNetwork(false);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setUserRole();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  setUserRole:function(){
    if (getApp().globalData.userInfo == null) {
      var _self = this;
      setTimeout(function () {
        _self.onShow();
      }, 1000);
      return;
    }
    this.setData({
      userRole: getApp().globalData.userInfo.usrRole
    });
  },
  gotoAddPoint:function(){
    wx.navigateTo({
      url: '/pages/viewpoint/addPoint',
    })
  },
  refresh:function(){
    this.requestNetwork(false);
  },
  loadMore:function(){
    this.requestNetwork(true);
  },
  requestNetwork: function (isLoadMore) {
    if (this.data.selectIndex == 0 && this.data.dkksIsNetWork) return;
    if (this.data.selectIndex == 1 && this.data.msjdIsNetWork) return;
    if (this.data.selectIndex == 2 && this.data.jpclIsNetWork) return;

    var obj = this.data.selectIndex == 0 ? this.data.dkksDatasObj : this.data.selectIndex == 1 ? this.data.msjdDatasObj : this.data.jpclDatasObj;
    if (isLoadMore && obj.hasNext != null && obj.hasNext != undefined && obj.hasNext == false) return;
    var pageNo = obj.pageNo;
    if (pageNo == null || pageNo == undefined || !isLoadMore) {
      pageNo = 1
    } else {
      pageNo = pageNo + 1;
    }
    var dataList = this.data.selectIndex == 0 ? this.data.dkksDatas : this.data.selectIndex == 1 ? this.data.msjdDatas : this.data.jpclDatas;
    if (!isLoadMore) pageNo = 1;
    if (dataList.length == 0) {
      if (this.data.selectIndex == 0) {
        this.setData({
          dkksIsShowLoading: true
        });
      } else if (this.data.selectIndex == 1) {
        this.setData({
          msjdIsShowLoading: true
        });
      } else {
        this.setData({
          jpclIsShowLoading: true
        });
      }
      this.setData({
        loadingMsg: "正在加载..."
      });
    }
    this.data.selectIndex == 0 ? this.data.dkksIsNetWork = true :
    this.data.selectIndex == 1 ? this.data.msjdIsNetWork = true : this.data.jpclIsNetWork = true;
    var _self = this;

    console.log((isLoadMore ? "加载更多:" : "刷新:") + pageNo);
    if (this.data.selectIndex == 0) {
      getDkksPage(pageNo, function (d) {
        _self.data.dkksIsNetWork = false;
        if (d) {
          if (d.data.result == 0 && _self.data.dkksDatas.length == 0) {
            _self.setData({
              dkksIsShowLoading: true,
              loadingMsg: "暂无数据"
            });
          } else {
            _self.setData({
              dkksIsShowLoading: false,
              loadingMsg: "",
              dkksDatas: isLoadMore ? _self.data.dkksDatas.concat(_self.parseDateTime(d.data.result)) : _self.parseDateTime(d.data.result),
              dkksDatasObj: d.data
            });
          }
        } else {
          _self.setData({ dkksIsShowLoading: false });
        }
      });
    } else if (this.data.selectIndex == 1) {
      getMsjdPage(pageNo, function (d) {
        _self.data.msjdIsNetWork = false;
        if (d) {
          if (d.data.result == 0 && _self.data.msjdDatas.length == 0) {
            _self.setData({
              msjdIsShowLoading: true,
              loadingMsg: "暂无数据"
            });
          } else {
            _self.setData({
              msjdIsShowLoading: false,
              loadingMsg: "",
              msjdDatas: isLoadMore ? _self.data.msjdDatas.concat(_self.parseDateTime(d.data.result)) : _self.parseDateTime(d.data.result),
              msjdDatasObj: d.data
            });
          }
        } else {
          _self.setData({ msjdIsShowLoading: false });
        }
      });    
    } else if (this.data.selectIndex == 2) {
      getJpclPage(pageNo, function (d) {
        _self.data.jpclIsNetWork = false;
        if (d) {
          if (d.data.result == 0 && _self.data.jpclDatas.length == 0) {
            _self.setData({
              jpclIsShowLoading: true,
              loadingMsg: "暂无数据"
            });
          } else {
            _self.setData({
              jpclIsShowLoading: false,
              loadingMsg: "",
              jpclDatas: isLoadMore ? _self.data.jpclDatas.concat(_self.parseDateTime(d.data.result)) : _self.parseDateTime(d.data.result),
              jpclDatasObj: d.data
            });
          }
        } else {
          _self.setData({ jpclIsShowLoading: false });
        }
      });         
    }

  },
  parseDateTime:function(d){
    var data = [];
    for (var i = 0; i < d.length; i++) {
      var obj = d[i];
      obj.opnPublishDate = timestampFormat(obj.opnPublishDate);
      data.push(obj);
    }
    return data;
  },
  /***
   * 顶部菜单左右滑动
   */
  changeItem:function(e){
    console.log(e.detail);
    this.setData({
      selectIndex:e.detail
    });
  },
  /***
   * 下面页面左右滑动
   */
  swiperChange: function (e) {
    this.data.selectIndex = e.detail.current;
    this.selectComponent("#scrollMenu").setSeIndex(this.data.selectIndex);
    var dataList = this.data.selectIndex == 0 ? this.data.dkksDatas : this.data.selectIndex == 1 ? this.data.msjdDatas : this.data.jpclDatas;
    if (dataList.length == 0) {
      this.requestNetwork(false);
    }
  },
})