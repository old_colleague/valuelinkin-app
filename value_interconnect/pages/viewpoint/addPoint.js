import { publishOpion } from '../../service/homeService.js';
import { timestampFormat, trim } from '../../utils/util.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    textValue:"",
    inputStockValue:"",
    inputValue:"",
    userRole:""
  },

  bindKeyText:function(e){
    this.data.textValue = e.detail.value;
  },
  bindKeyStockInput:function(e){
    this.data.inputStockValue = e.detail.value;
  },
  bindKeyInput:function(e){
    this.data.inputValue = e.detail.value;
  },
  save:function(){
    var isStock = false;
    if (this.data.userRole == "TG" || this.data.userRole == "FXS"){ //投顾和分析是需要填写股票代码
      isStock = true;
    }
    var msg = "";
    if (trim(this.data.inputValue).length == 0) {
      msg = "请输入标题";
    } else if (trim(this.data.textValue).length == 0) {
      msg = "请输入内容";
    } else if (trim(this.data.inputStockValue).length == 0 && isStock) {
      msg = "请输入股票代码";
    }    
    if (msg !=""){
      wx.showToast({
        title: msg,
        icon: 'none',
        duration: 2000
      });
      return;
    } 
    wx.showLoading({
      title: '请稍后...',
    })
    var _self = this;
    publishOpion(this.data.inputValue, this.data.textValue, this.data.inputStockValue, function (d) {
      wx.hideLoading();
      if (d) {
        wx.showToast({
          title: "发布成功",
          icon: 'none',
          duration: 1000
        });
        wx.navigateBack({
          delta:1
        })
      }
    });

    // console.log(userInfo);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (getApp().globalData.userInfo == null){
      var _self = this;
      setTimeout(function(){
        _self.onShow();
      },1000);
      return;
    }
    this.setData({
      userRole: getApp().globalData.userInfo.usrRole
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})