import { getPageSug, publicSuggestion } from '../../../service/myService.js';
import { timestampFormat } from '../../../utils/util.js';



Page({

  /**
   * 页面的初始数据
   */
  data: {
    isNetwork: false,
    isLoadding: false,
    datas: [],
    dataPageObj: {},
    loadingMsg: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.requestNetwork(false);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.requestNetwork(false);
  },
  refreshProblemList:function(){
    this.requestNetwork(false);
  },
  requestNetwork: function (isLoadMore) {
    if (this.data.isNetwork) {
      wx.stopPullDownRefresh();
      return;
    }
    var obj = this.data.dataPageObj;
    if (isLoadMore && obj.hasNext != null && obj.hasNext != undefined && obj.hasNext == false) return;
    var pageNo = obj.pageNo;
    if (pageNo == null || pageNo == undefined || !isLoadMore) {
      pageNo = 1
    } else {
      pageNo = pageNo + 1;
    }
    var dataList = this.data.datas;
    if (!isLoadMore) pageNo = 1;
    if (dataList.length == 0) {
      this.setData({
        isLoadding: true,
        loadingMsg: "正在加载..."
      });
    }
    this.data.isNetwork = true;
    var _self = this;
    console.log((isLoadMore ? "加载更多:" : "刷新:") + pageNo);
    getPageSug(pageNo, function (d) {
      wx.stopPullDownRefresh();
      _self.data.isNetwork = false;
      if (d) {
        if (d.data.result == 0 && _self.data.datas.length == 0) {
          _self.setData({
            isLoadding: true,
            loadingMsg: "暂无数据"
          });
        } else {
          var data = [];
          for (var i = 0; i < d.data.result.length; i++) {
            var obj = d.data.result[i];
            obj.createdOn = timestampFormat(obj.createdOn);
            obj.qsnStatus = obj.sgsStatus =="CLOSE"?1:0;
            obj.qsnContent = obj.sgsContent;
            obj.ansContent = obj.sgsReply;
            data.push(obj);
          }
          _self.setData({
            isLoadding: false,
            loadingMsg: "",
            datas: isLoadMore ? _self.data.datas.concat(data) : data,
            dataPageObj: d.data
          });
        }
      } else {
        _self.setData({ isLoadding: false });
      }
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.requestNetwork(true);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})