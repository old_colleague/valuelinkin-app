
import { getMyStockPage } from '../../../service/homeService.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    isShow: false,
    loaddingMessage: "正在加载..."
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var _self = this;
    if (this.data.list.length == 0){
      _self.setData({
        isShow: true,
        loaddingMessage: "正在加载..."
      });
    }
    getMyStockPage(function(d){
      wx.stopPullDownRefresh();
      if (d){
          if (d.data.length == 0){
            _self.setData({
              isShow: true,
              list: [],
              loaddingMessage: "暂无订阅数据"
            });
          }else{
            _self.setData({
              isShow: false,
              list: d.data
            }); 
          }
      }else if (_self.data.list.length == 0) {
        _self.setData({
          isShow: true,
          loaddingMessage: "加载失败"
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onShow();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})