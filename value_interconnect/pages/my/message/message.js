import { getMsgPage } from '../../../service/myService.js';
import { timestampFormat } from '../../../utils/util.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    hasNext:true,
    pageNo:0,
    list: [],
    isNetWork:false,
    loaddingMessage:"正在加载...",
    isShow:false
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.requestNetwork(false);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  
  /**
   * 下拉刷新
   */
  onPullDownRefresh:function(){
    
  },
  onReachBottom:function(){
    console.log("1");
  },
  requestNetwork:function(isLoadMore){
    if (this.data.isNetWork) return;
    if (this.data.list.length == 0){
      this.setData({
        loaddingMessage: "正在加载...",
        isShow: true
      });
    }
    if (isLoadMore && !this.data.hasNextPage) return;//表示没有下一页
    if (!isLoadMore) this.data.pageNo = 0;
    this.data.isNetWork = true;
    var _self = this;
    this.data.pageNo = this.data.pageNo + 1;
    getMsgPage(this.data.pageNo,function(d){
      _self.data.isNetWork = false;
      if (d){
        _self.data.hasNext = d.data.hasNext;
        
        if (!isLoadMore) _self.data.list = [];
        _self.data.list = _self.data.list.concat(d.data.result);
        _self.setData({
          isShow: false,
          list: _self.data.list
        });  
      }else if (_self.data.list.length == 0){
        _self.setData({
          loaddingMessage: "加载失败",
          isShow: true
        });          
      }
    });

    // wx.stopPullDownRefresh();
  }
})