import { sendSmsCode, savePhone } from '../../../service/myService.js';
import { trim } from '../../../utils/util.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    sendText:"发送",
    inputPhone:null,
    inputCode: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindKeyInputPhone: function(e){
    this.data.inputPhone = e.detail.value;
  },

  bindKeyInputCode: function(e){
    this.data.inputCode = e.detail.value;
  },
  /**
   * 保存绑定的手机号码
   */
  save: function(){
    if (trim(this.data.inputPhone).length == 0){
      wx.showToast({
        title: "请输入手机号码",
        icon: 'none',
        duration: 2500
      });  
      return;      
    } else if (trim(this.data.inputPhone).length < 11){
      wx.showToast({
        title: "请输入正确手机号码",
        icon: 'none',
        duration: 2500
      });    
      return;    
    } else if (trim(this.data.inputCode).length == 0){
      wx.showToast({
        title: "请输入验证码",
        icon: 'none',
        duration: 2500
      });
      return;        
    }
    var _self = this;
    savePhone(function(d){
      if (d){
        wx.showToast({
          title: '保存成功',
          icon: 'none',
          duration: 2000
        })
        //替换用户个人信息中的手机号码，然后回退。
        var app = getApp();
        app.globalData.userInfo.usrPhone = _self.data.inputPhone;
        wx.navigateBack({
          delta: 1
        })
      }
    }, trim(this.data.inputPhone), trim(this.data.inputCode));
  },
  /**
   * 发送验证码
   */
  sendSms: function(){
    var _self = this;
    if (this.data.sendText == "发送"){
      if (trim(this.data.inputPhone).length == 0) {
        wx.showToast({
          title: "请输入手机号",
          icon: 'none',
          duration: 2500
        });
        return;
      }
      sendSmsCode(function (d) {
        if (d) {
          wx.showToast({
            title: '发送成功',
            icon: 'none',
            duration: 2000
          })
          _self.countDown(60); 
        }
      }, this.data.inputPhone);
    }
  },
  countDown: function(num){
    if (num < 0) {
      this.setData({
        sendText: "发送"
      });
      return;
    }
    num = num - 1;
    this.setData({
      sendText:num+" s"
    });
    var _self = this;
   setTimeout(function(){
     _self.countDown(num);
   },1000);
  }
})