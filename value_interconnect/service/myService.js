import { requestNetWork, getToken, request } from './requestNetwork.js';


const getUserInfo = function (callback) {
  request("user/getUserByToken", "GET", { "token":getToken()}, callback);
};

const setUserInfo = function (parm, callback){
  request("user/updateUser", "POST", parm, callback);
}

const sendSmsCode = function (callback, phoneNo){
  requestNetWork("sms/getVerifyCode", "GET", { "phoneNo": phoneNo }, callback);
}

const savePhone = function (callback, phoneNo, verifyCode){
  requestNetWork("user/updatePhoneNo", "POST", { "phoneNo": phoneNo, "verifyCode": verifyCode }, callback);
}

const getMsgPage = function (pageNO,callback){
  requestNetWork("appmsg/getPage", "GET", { "currentPage": pageNO, "pageSize": 10 }, callback);
}
const getUsrUnReadCount = function (callback) {
  requestNetWork("appmsg/getUsrUnReadCount", "GET", {}, callback);
}

const getMyQuestionPage = function (pageNO,callback) {
  requestNetWork("question/getPage", "GET", { "currentPage": pageNO, "pageSize": 10 }, callback);
}

const getPageForNwsId = function (pageNO, nwsId, callback) {
  requestNetWork("question/getPageForNwsId", "GET", { "nwsId": nwsId, "currentPage": pageNO, "pageSize": 10 }, callback);
}

const saveQuestion = function (nwsId, qsnContent, callback) {
  requestNetWork("question/saveQuestion", "POST", { "nwsId": nwsId, "qsnContent": qsnContent }, callback);
}

const findMyOpnList = function (pageNO, callback) {
  requestNetWork("opinion/findMyOpnList", "GET", { "currentPage": pageNO, "pageSize": 10 }, callback);
}

const publicSuggestion = function (sgsContent, callback) {
  requestNetWork("suggestion/publicSuggestion", "POST", { "sgsContent": sgsContent}, callback);
}
 
const getPageSug = function (pageNO, callback) {
  requestNetWork("suggestion/getPageSug", "GET", { "currentPage": pageNO, "pageSize": 10 }, callback);
}
module.exports = {
  getUserInfo: getUserInfo,
  sendSmsCode: sendSmsCode,
  savePhone: savePhone,
  getMsgPage: getMsgPage,
  setUserInfo: setUserInfo,
  getUsrUnReadCount: getUsrUnReadCount,
  getMyQuestionPage: getMyQuestionPage,
  saveQuestion: saveQuestion,
  getPageForNwsId: getPageForNwsId,
  findMyOpnList: findMyOpnList,
  getPageSug,
  publicSuggestion
}
