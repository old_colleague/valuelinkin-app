
const baseURL = "https://apis.valuelinkin.com/";

const getToken = function(){
  return wx.getStorageSync('token');
};

const setToken = function (token) {
  wx.setStorageSync('token', token);
};

const wxLogin = function (code,callback){
  var requestUrl = baseURL + "user/loginByWx";
  wx.request({
    url: requestUrl,
    data: { "code": code},
    method: "POST",
    dataType: "json",
    success(res) {
      console.log("微信授权登陆返回数据："+JSON.stringify(res));
      if (res.statusCode == 200 && res.data.code == 0) {
        setToken(res.data.data.loginToken);
        callback(res.data);
      } else {
        callback(null);
      }
    },
    fail() {
      callback(null);
    },
  });
}


const request = function (url, method, parm, callback){
  var requestUrl = baseURL + url;
  wx.request({
    url: requestUrl,
    data: parm,
    method: method,
    dataType: "json",
    header: {
      "token": getToken()
    },
    success(res) {
      console.log("请求返回数据：" + JSON.stringify(res));
      if (res.statusCode == 200 && res.data.code == 0) {
        callback(res.data);
      } else if (res.statusCode == 200 && res.data.code == -999) {
        //调用微信授权登陆 
        console.log("调用微信授权登陆 ");
        const app = getApp();
        app.wxLogin(function (data) {
          if (data != null) {
            console.log("微信授权登陆成功 ");
            requestNetWork(url, method, parm, callback);
          } else {
            callback(null);
          }
        });
      } else {
        callback(null);
        wx.showToast({
          title: res.data.msg != null && res.data.msg != undefined && res.data.msg != "" ? res.data.msg : "请求失败",
          icon: 'none',
          duration: 2000
        });
      }
    },
    fail() {
      wx.showToast({
        title: '请求失败',
        icon: 'none',
        duration: 2000
      });
    },
  });
}

var requestCount = 0;
const requestNetWork = function (url, method,parm,callback){
  if (getApp().globalData.userInfo == null){ //如果用户信息为空，则用户未登录，或者正在登陆，则轮询检查用户登陆情况。5次之后还是为空则返回用户登陆失败。
    if (requestCount == 5) {
      requestCount = 0;
      callback(null);return;
    }
    setTimeout(function(){
      requestNetWork(url, method, parm, callback);
      requestCount++;
    },1000);
  }else{
    requestCount = 0;
    request(url, method, parm, callback);
  }
}

module.exports = {
  requestNetWork: requestNetWork,
  getToken: getToken,
  wxLogin: wxLogin,
  request: request
}
