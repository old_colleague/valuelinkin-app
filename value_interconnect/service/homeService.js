import { requestNetWork } from './requestNetwork.js';

const queryStockGroupPage = function (callback) {
  requestNetWork("stockgroup/queryStockGroupPage", "GET", {  }, callback);
}

const getMyStockPage = function (callback) {
  requestNetWork("stockgroup/getMyStock", "GET", {}, callback);
}

const getNewsflushPage = function (pageNO, callback){
  requestNetWork("newsflush/getPage", "GET", {currentPage: pageNO, pageSize:10}, callback);
}
 
const getNewsflushDetail = function (newsId, callback) {
  requestNetWork("newsflush/getNwsContent", "GET", { nwsId: newsId }, callback);
}

const deleteStockWacth = function (stockCode, callback) {
  requestNetWork("stockgroup/deleteStockWacth", "POST", { stkCode: stockCode }, callback);
}
const addStockWacth = function (stockCode, callback) {
  requestNetWork("stockgroup/addStockWacth", "POST", { stkCode: stockCode }, callback);
}

const getPageByStock = function (pageNO,stockCode, callback) {
  requestNetWork("newsflush/getPageByStock", "GET", { pageSize: 10, currentPage: pageNO, stkCode: stockCode }, callback);
}
const getNwsContent = function (stockCode, callback) {
  requestNetWork("stockfinancialdata/getNwsContent", "GET", { stkCode: stockCode }, callback);
}
const getStockJPCL = function (pageNO, stockCode, callback) {
  requestNetWork("stockfinancialdata/getStockJPCL", "GET", { pageSize: 10, currentPage: pageNO, stkCode: stockCode }, callback);
}

const getStockReport = function (pageNO, stockCode, callback) {
  requestNetWork("stockfinancialdata/getStockReport", "GET", { pageSize: 10, currentPage: pageNO, stkCode: stockCode }, callback);
}
 
const getStockMg = function (stockCode, callback) {
  requestNetWork("stockfinancialdata/getStockMg", "GET", { stkCode: stockCode }, callback);
}

const getStockShareholding = function (stockCode, callback) {
  requestNetWork("stockfinancialdata/getStockShareholding", "GET", { stkCode: stockCode }, callback);
}

const getStockReportDesc = function (id, callback) {
  requestNetWork("stockfinancialdata/getStockReportDesc", "GET", { id: id }, callback);
}

const getDkksPage = function (currentPage, callback) {
  requestNetWork("opinion/getDkksPage", "GET", { currentPage: currentPage, pageSize: 10 }, callback);
}

const getJpclPage = function (currentPage, callback) {
  requestNetWork("opinion/getJpclPage", "GET", { currentPage: currentPage, pageSize: 10 }, callback);
}

const getMsjdPage = function (currentPage, callback) {
  requestNetWork("opinion/getMsjdPage", "GET", { currentPage: currentPage, pageSize:10 }, callback);
}

const getOpnContent = function (opnId, callback) {
  requestNetWork("opinion/getOpnContent", "GET", { opnId: opnId }, callback);
}

const publishOpion = function (opnTitle, opnContent, opnStockCode, callback) {
  var parm = { opnTitle: opnTitle, opnContent: opnContent };
  if (opnStockCode != null && opnStockCode != ""){
    parm.opnStockCode = opnStockCode;
  }
  requestNetWork("opinion/publishOpion", "POST", parm, callback);
}

const getGgxxPage = function (pageNO, callback) {
  requestNetWork("opinion/getGgxxPage", "GET", { pageSize: 10, currentPage: pageNO }, callback);
}

const getAdsPage = function (type, callback) {
  requestNetWork("ads/getPage", "GET", { pageSize: 100, currentPage: 1, adsLocation: type }, callback);
}

const findIndexData = function (callback) {
  requestNetWork("index/findIndexData", "GET", {}, callback);
}

const getZhiBoAllPage = function (callback) {
  requestNetWork("newslive/getZhiBoAllPage", "GET", {}, callback);
}

const getIndexData = function (callback) {
  requestNetWork("opinion/getIndexData", "GET", {}, callback);
}




module.exports = {
  queryStockGroupPage: queryStockGroupPage,
  getNewsflushPage: getNewsflushPage,
  getNewsflushDetail: getNewsflushDetail,
  getMyStockPage: getMyStockPage,
  deleteStockWacth: deleteStockWacth,
  addStockWacth: addStockWacth,
  getPageByStock: getPageByStock,
  getNwsContent: getNwsContent,
  getStockJPCL: getStockJPCL,
  getStockReport: getStockReport,
  getStockMg: getStockMg,
  getStockShareholding: getStockShareholding,
  getStockReportDesc: getStockReportDesc,
  getDkksPage: getDkksPage,
  getJpclPage: getJpclPage,
  getMsjdPage: getMsjdPage,
  getOpnContent: getOpnContent,
  publishOpion: publishOpion,
  getGgxxPage: getGgxxPage,
  getAdsPage: getAdsPage,
  findIndexData: findIndexData,
  getZhiBoAllPage: getZhiBoAllPage,
  getIndexData: getIndexData
}

