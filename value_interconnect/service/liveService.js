import { requestNetWork, getToken, request } from './requestNetwork.js';


const getJchgPage = function (pageNo,callback) {
  request("newslive/getJchgPage", "GET", { currentPage: pageNo, pageSize:10 }, callback);
}

const getSsgszbYgPage = function (pageNo, callback) {
  request("newslive/getSsgszbYgPage", "GET", { currentPage: pageNo, pageSize: 10 }, callback);
}

const getTzhdYgPage = function (pageNo, callback) {
  request("newslive/getTzhdYgPage", "GET", { currentPage: pageNo, pageSize: 10 }, callback);
}

const getSsgszbZbPage = function (callback) {
  request("newslive/getSsgszbZbPage", "GET", {  }, callback);
}

const getTzhdZbPage = function ( callback) {
  request("newslive/getTzhdZbPage", "GET", {}, callback);
}

const getNwsLiveContent = function (nwsId, callback) {
  request("newslive/getNwsLiveContent", "GET", { nwsId: nwsId}, callback);
}

const getRelationVideo = function (nwsId, callback) {
  var parm =  {};
  if (nwsId != "" && nwsId != null) parm.stkCode = nwsId;
  request("newslive/getRelationVideo", "GET", parm, callback);
}


module.exports = {
  getJchgPage: getJchgPage,
  getSsgszbYgPage: getSsgszbYgPage,
  getTzhdYgPage: getTzhdYgPage,
  getSsgszbZbPage: getSsgszbZbPage,
  getTzhdZbPage: getTzhdZbPage,
  getNwsLiveContent: getNwsLiveContent,
  getRelationVideo: getRelationVideo
}
