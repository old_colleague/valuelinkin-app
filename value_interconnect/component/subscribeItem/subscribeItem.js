// component/subscribeItem/subscribeItem.js
import { deleteStockWacth, addStockWacth } from '../../service/homeService.js';

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item: {
      type: Object,
      value: {},
    },
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    gotoDetil:function(){
      wx.navigateTo({
        url: '/pages/home/homeDetail?stkCode=' + this.data.item.stkCode,
      })
    },
    /***
     * 订阅
     */
    subscribeltem:function(){
      var _self = this;
      if (this.data.item.watchFlag == "Y"){ //表示已订阅，再次点击就取消订阅
        deleteStockWacth(this.data.item.stkCode,function(d){
            if (d){
              wx.showToast({
                title: '取消成功',
                icon: 'none',
                duration: 2000
              });
              _self.data.item.watchFlag = "N";
              _self.setData({
                item: _self.data.item
              });
            }
        });
      }else{ //订阅
        addStockWacth(this.data.item.stkCode, function (d) {
          if (d) {
            wx.showToast({
              title: '订阅成功',
              icon: 'none',
              duration: 2000
            });
            _self.data.item.watchFlag = "Y";
            _self.setData({
              item: _self.data.item
            });            
          }
        });
      }
    }
  }
})
