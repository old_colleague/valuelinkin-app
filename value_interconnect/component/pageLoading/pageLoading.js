// component/pageLoading/pageLoading.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    innerText: {
      type: String,
      value: '正在加载...',
    },
    lodingType:{ // 0 正在加载，1 加载失败，
      type:Number,
      value:0
    },
    isShow:{ //0不显示，1显示
      type: Boolean,
      value: 0      
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
