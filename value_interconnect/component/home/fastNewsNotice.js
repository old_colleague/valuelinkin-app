// component/home/fastNewsNotice.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isShowTime:{
      type:Boolean,
      value: true,
    },
    item:{
      type:Object,
      value: {},
    },
    type: {//0快讯公告，nwsId  1 金牌策略，opnId 2研究报告 id
      type: Number,
      value: 0,      
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    gotoDetail:function(){
      var id = this.data.type == 0 ? this.data.item.nwsId : this.data.type == 1 ? this.data.item.opnId : this.data.type == 2 ? this.data.item.id:0;
      if (this.data.type == 1){
        wx.navigateTo({
          url: '/pages/viewpoint/viewpointDetail?opnId=' + id
        })        
      }else{
        wx.navigateTo({
          url: '/pages/home/newsDetail?nwsId=' + id + "&type=" + (parseInt(this.data.type) + 1),
        })
      }
    }
  }
})
