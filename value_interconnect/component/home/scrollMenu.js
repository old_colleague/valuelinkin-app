// component/home/scrollMenu.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    headItem: { //标题
      type: Array,
      value:[],
    },
    isShowBorder:{
      type:Boolean,
      value:true
    },
    styleType:{ // 0默认样式，1黑灰色样式
      type: Number,
      value: true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    selectIndex:0,
    nodeWidth:0,
    windowWidth:wx.getSystemInfoSync().windowWidth,
    nodeSip:20,
    countWidth:0,
    scrollLeft:0,
  },
  ready:function() {
    this.setHeadeNodeWidth();
  },
  
  /**
   * 组件的方法列表
   */
  methods: {
    //设置头部宽度
    setHeadeNodeWidth:function(){
      var _self = this;
      var query = this.createSelectorQuery();
      query.selectAll('.h_s_h_item').boundingClientRect();
      query.exec((res) => {
        var maxWidth = 0;
        for (var i = 0; i < (res[0]).length; i++) {
          var width = res[0][i].width;
          console.log(res[0]);
          maxWidth = maxWidth + width;
        }
        var width = maxWidth + this.data.nodeSip * ((res[0]).length + 1);
        this.data.countWidth = width;
        if (width - 30 < this.data.windowWidth) {
          _self.setData({
            nodeSip: (this.data.windowWidth - maxWidth) / ((res[0]).length + 1)
          });
        }
      });
    },
    /***
     * 节点选中
     */
    onItemClick:function(e){
      var selectIndex = e.target.dataset.index;
      this.setSeIndex(selectIndex);
      this.triggerEvent('changeItem', selectIndex);//调用父组件，通知selectIndex 改变。
    },
    setSeIndex:function(index){
      this.setData({
        selectIndex: index
      });
      if ((this.data.countWidth - 30) > this.data.windowWidth) {//当item节点总宽度超过整个屏幕宽度时需要调整item位置。
        var _self = this;
        var query = this.createSelectorQuery();
        query.selectAll('.h_s_h_item').boundingClientRect();
        query.exec((res) => {
          var width = res[0][_self.data.selectIndex].left -100;
          _self.setData({
            scrollLeft: Math.ceil(width)
          })
        });
      }
    }
  }
})
