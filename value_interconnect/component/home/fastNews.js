// component/home/fastNews.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    objects: { //标题
      type: Object,
      value: {},
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    gotoDetail:function(e){
      var obj = this.data.objects.list[e.currentTarget.dataset.index];
      wx.navigateTo({
        url: '/pages/home/newsDetail?nwsId=' + obj.nwsId+"&type=0",
      })
    }
  }
})
