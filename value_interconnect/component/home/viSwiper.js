// component/home/viSwiper.js
import { getAdsPage } from '../../service/homeService.js';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    adType: { //标题
      type: String,
      value: "",
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    adList: []
  },
  ready: function () {
    this.getAds();
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getAds() {
      var _self = this;
      getAdsPage(this.data.adType, function (d) {
        _self.setData({
          adList: d.data.result
        });
      });
    },
    tapItemAd: function (event) {
      let adObj = this.data.adList[event.currentTarget.dataset.index];
      if (adObj.adsActionType == 'href') {
        if (adObj.adsActionLink != null && adObj.adsActionLink != undefined && adObj.adsActionLink !=""){
          wx.navigateTo({
            url: '/pages/webView/index?adUrl=' + adObj.adsActionLink
          });
        }
      } else if (adObj.adsActionType == 'mui') {
        let obj = JSON.parse(adObj.adsActionLink);
        if (obj.type == 'live') {
          var type = obj.nwsStatus == 2 || obj.nwsStatus == 3 ? 2 : obj.nwsStatus;
          wx.navigateTo({
            url: '/pages/live/liveDetail?nwsId=' + obj.nwsId + "&type=" + type
          })
        }

      }
    }
  }
})
