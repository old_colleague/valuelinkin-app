// component/newPage/home/recommend.js
import { timestampFormat } from '../../../utils/util.js';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item:{
      type:Object,
      value:{}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    itemObj:{},
  },
  ready:function(){
    let item = {};
    item.nwsThumbnailUrl = this.data.item.nwsThumbnailUrl;
    item.nwsTitle = this.data.item.nwsTitle;
    item.nwsViewCount = this.data.item.nwsViewCount;
    item.nwsPublishDate = timestampFormat(this.data.item.nwsPublishDate);
    this.setData({
      itemObj:item
    });
  },
  /**
   * 组件的方法列表
   */
  methods: {
    gotoLive:function(){
      var type = this.data.item.nwsStatus == 2 || this.data.item.nwsStatus == 3 ? 2 : this.data.item.nwsStatus;
      wx.navigateTo({
        url: '/pages/live/liveDetail?nwsId=' + this.data.item.nwsId + "&type=" + type
      })
    }
  }
})
