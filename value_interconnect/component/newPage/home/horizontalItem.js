// component/newPage/home/horizontalItem.js
import { timestampFormat } from '../../../utils/util.js';
Component({
  /**
    * 组件的属性列表
    */
  properties: {
    item: {
      type: Object,
      value: {}
    },
    type:{
      type:Number,
      value:0 , //0公告秀秀1 直播
    },
    left:{
      type: Number,
      value: 0, 
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    itemObj: {},
  },
  ready: function () {
    let item = {};
    if (this.data.type == 0){
      let img = this.data.item.opnThumbnailUrl;
      item.nwsThumbnailUrl = img == null || img == undefined ||  img == "" ? '/img/newImg/ggxx.png' : img;
      item.nwsTitle = this.data.item.opnTitle;
      item.nwsViewCount = this.data.item.opnViewCount;
      item.nwsPublishDate = timestampFormat(this.data.item.opnPublishDate);
    } else if (this.data.type == 1){
      item.nwsThumbnailUrl = this.data.item.nwsThumbnailUrl;
      item.nwsTitle = this.data.item.nwsTitle;
      item.nwsViewCount = this.data.item.nwsViewCount;
      item.nwsPublishDate = timestampFormat(this.data.item.nwsPublishDate);
    }
    this.setData({
      itemObj: item
    });
  },

  /**
   * 组件的方法列表
   */
  methods: {
    gotoDetail: function (e) {
      if (this.data.type == 0){
        var obj = this.data.item;
        wx.navigateTo({
          url: '/pages/viewpoint/viewpointDetail?opnId=' + obj.opnId
        })
      }else if (this.data.type == 1){
        let obj = this.data.item;
        var type = obj.nwsStatus == 2 || obj.nwsStatus == 3 ? 2 : obj.nwsStatus;
        wx.navigateTo({
          url: '/pages/live/liveDetail?nwsId=' + obj.nwsId + "&type=" + type
        })
      }

    }
  }
})
