// component/newPage/home/viewpointItem.js
import { timestampFormat } from '../../../utils/util.js';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item: {
      type: Object,
      value: {}
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    itemObj: {},
  },
  ready: function () {
    let item = {};
    item.nwsThumbnailUrl = this.data.item.opnThumbnailUrl;
    item.nwsTitle = this.data.item.opnTitle;
    item.nwsViewCount = this.data.item.opnViewCount;
    item.nwsPublishDate = timestampFormat(this.data.item.opnPublishDate);
    this.setData({
      itemObj: item
    });
  },
  /**
   * 组件的方法列表
   */
  methods: {
    gotoDetail:function(){
      var obj = this.data.item;
      wx.navigateTo({
        url: '/pages/viewpoint/viewpointDetail?opnId=' + obj.opnId
      })
    }
  }
})
