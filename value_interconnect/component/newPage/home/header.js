// component/newPage/home/header.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    titles:{
      type:Array,
      value:[]
    },
    type:{ //0首页 1 直播 2上市公司 3观点。 
      type:Number,
      type:0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    gotoDetail:function(e){
      let index = e.currentTarget.dataset.index;
      if(this.data.type == 2){
        if (this.data.titles[index] == '快讯'){
          wx.navigateTo({
            url: '/pages/home/home?index=1'
          })
        } else  if (this.data.titles[index] == '公告') {
          wx.navigateTo({
            url: '/pages/home/home?index=0'
          })
        }
      } else if (this.data.type == 1){
        if (this.data.titles[index] == '公司直播') {
          wx.navigateTo({
            url: '/pages/live/live?index=0'
          })
        } else if (this.data.titles[index] == '粒场直播') {
          wx.navigateTo({
            url: '/pages/live/live?index=1'
          })
        } else if (this.data.titles[index] == '精彩回看') {
          wx.navigateTo({
            url: '/pages/live/live?index=2'
          })
        }
      }else if (this.data.type = 3){
        if (this.data.titles[index] == '专家') {
          wx.navigateTo({
            url: '/pages/viewpoint/viewpoint?index=1'
          })
        } else if (this.data.titles[index] == '大咖') {
          wx.navigateTo({
            url: '/pages/viewpoint/viewpoint?index=0'
          })
        } else if (this.data.titles[index] == '投顾') {
          wx.navigateTo({
            url: '/pages/viewpoint/viewpoint?index=2'
          })
        }
      }
    }
  }
})
