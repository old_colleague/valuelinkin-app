// component/newPage/home/moreHeader.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title:{
      type: String,
      value: '',
    },
    type:{
      type:Number,
      value:0, //0首页 1 直播 2 观点
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    gotoList:function(){
      if (this.data.title == "公告秀秀"){
        wx.navigateTo({
          url: '/pages/home/home?index=0'
        })
      }
      if (this.data.type == 1){
        if (this.data.title == "直播预告") {
          wx.navigateTo({
            url: '/pages/live/live?index=0'
          })
        } else if (this.data.title == "直播回看") {
          wx.navigateTo({
            url: '/pages/live/live?index=2'
          })
        }
      }else if (this.data.type == 2){
        if (this.data.title == '专家') {
          wx.navigateTo({
            url: '/pages/viewpoint/viewpoint?index=1'
          })
        } else if (this.data.title == '大咖') {
          wx.navigateTo({
            url: '/pages/viewpoint/viewpoint?index=0'
          })
        } else if (this.data.title == '投顾') {
          wx.navigateTo({
            url: '/pages/viewpoint/viewpoint?index=2'
          })
        }
      }
    }
  }
})
