// component/tabBarPage/homePage/index.js
import storeData from '../storeData.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    selectIndex:0,
    isShow:false,
    loaddingMessage:'正在加载...',
    nodeItem:[
      {
        "imgUrl":'/img/newImg/shangshi.png',
        "text":'上市公司'
      },
      {
        "imgUrl": '/img/newImg/tougu.png',
        "text": '投顾策略'
      },
      {
        "imgUrl": '/img/newImg/shiping.png',
        "text": '精选视频'
      },
      {
        "imgUrl": '/img/newImg/gonggao.png',
        "text": '公告秀秀'
      }
    ],
    publicCompany:[],
    recommendList:[],
    announceList:[],
  },

  /**
   * 组件的方法列表
   */
  ready: function () {
    this.getData();
  },
  methods: {
    tapItem:function(event){
      let index = event.currentTarget.dataset.index;
      if (index == 0){
        this.triggerEvent('changeTab', 2);
      }else if (index == 1){
        this.triggerEvent('changeTab', 3);        
      }else if (index == 2){
        wx.navigateTo({
          url: '/pages/live/live?index=2'
        })
      }else if (index == 3){
        wx.navigateTo({
          url: '/pages/home/home?index=0'
        })        
      }
    },
    tapPublicCompanyItem:function(event){
      console.log(event.currentTarget.dataset.index);
    },
    gotoLive:function(e){
      let index = e.currentTarget.dataset.index;
      let obj = this.data.publicCompany[index];
      var type = obj.nwsStatus == 2 || obj.nwsStatus == 3 ? 2 : obj.nwsStatus;
      wx.navigateTo({
        url: '/pages/live/liveDetail?nwsId=' + obj.nwsId + "&type=" + type
      })
    },
    getData:function(){
      if (storeData.getHomeData() == null || (storeData.getHomeData() != null && storeData.getHomeData().code != 0)){
        this.setData({
          isShow: true,
          loaddingMessage: '正在加载...'
        });
      }else{
        this.setData({
          isShow: false,
          publicCompany: storeData.homeData.data.liveList,
          recommendList: storeData.homeData.data.recommendList,
          announceList: storeData.homeData.data.announceList
        });  
      }
      let _self = this;
      storeData.getHomeDataRequest(function(d){
        if (d && d.code != 0){
          _self.setData({
            isShow: true,
            loaddingMessage: '加载失败'
          });
        }else{
          _self.setData({
            isShow: false,
            publicCompany: d.data.liveList,
            recommendList: d.data.recommendList,
            announceList: d.data.announceList
          });          
        }
      });
    }
  }
})
