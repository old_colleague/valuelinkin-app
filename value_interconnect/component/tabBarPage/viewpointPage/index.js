// component/tabBarPage/viewpointPage/index.js
import storeData from '../storeData.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    
  },

  /**
   * 组件的初始数据
   */
  data: {
    dkList: [],
    tgList: [],
    zjList: [],
    isShow: false,
    loaddingMessage: '正在加载...'
  },
  ready: function () {
    this.getData();
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getData: function () {
      let _self = this;
      if (storeData.viewpointData == null ||
        (storeData.viewpointData != null && storeData.viewpointData.code != 0)) {

        this.setData({
          isShow: true,
          loaddingMessage: '正在加载...'
        });
      } else {
        _self.setData({
          isShow: false,
          dkList: storeData.viewpointData.data.dkList,
          tgList: storeData.viewpointData.data.tgList,
          zjList: storeData.viewpointData.data.zjList,
        });
      }
      storeData.requestViewpoint(function (d) {
        if (d && d.code != 0) {
          _self.setData({
            isShow: true,
            loaddingMessage: '加载失败'
          });
        } else {
          _self.setData({
            isShow: false,
            dkList: d.data.dkList,
            tgList: d.data.tgList,
            zjList: d.data.zjList,
          });
        }
      });
    }
    // 
  }
})
