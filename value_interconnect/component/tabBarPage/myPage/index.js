import { getUserInfo, getUsrUnReadCount } from '../../../service/myService.js';
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    userInfo: null,
    getRoleStr: "",
    isShowMessageRed: false,
  },
  attached: function(){
    this.requestUserInfo();
    this.getUsrUnRead();
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getUsrUnRead: function () {
      var _self = this;
      getUsrUnReadCount(function (d) {
        _self.setData({
          isShowMessageRed: d.data.UsrUnReadCount > 0 ? true : false
        })
      });
    },
    requestUserInfo: function () {
      var _self = this;
      if (getApp().globalData.userInfo == null) {
        wx.showLoading({
          title: '加载中...',
        })
        getUserInfo(function (data) {
          wx.hideLoading();
          if (data) {
            getApp().globalData.userInfo = data.data;
            var userInfo = data.data;
            _self.setData({
              userInfo: userInfo,
              getRoleStr: getApp().globalData.getRoleStr[userInfo.usrRole]
            })
          }
        });

      } else {
        var userInfo = getApp().globalData.userInfo;
        this.setData({
          userInfo: userInfo,
          getRoleStr: getApp().globalData.getRoleStr[userInfo.usrRole]
        })
      }
    },
    personInfoAction: function () {
      wx.navigateTo({
        url: '/pages/my/personInfo'
      })
    },
    tapNode: function (e) {
      var index = parseInt(e.currentTarget.dataset.index);
      if (index == 0) {//消息中心
        wx.navigateTo({
          url: '/pages/my/message/message'
        })
      } else if (index == 1) { //我的订阅
        wx.navigateTo({
          url: '/pages/my/subscribe/subscribe'
        })
      } else if (index == 2) {//我的提问
        if (this.data.userInfo.usrRole == "TG" || this.data.userInfo.usrRole == "DK" || this.data.userInfo.usrRole == "FXS") {
          wx.navigateTo({
            url: '/pages/my/myViewpoint/myViewpoint'
          })
        } else {
          wx.navigateTo({
            url: '/pages/my/myProblem/myProblem'
          })
        }
      } else if (index == 3) {//关于我们
        wx.navigateTo({
          url: '/pages/my/about/about'
        })
      } else if (index == 4) {//意见反馈
        wx.navigateTo({
          url: '/pages/my/feedback/feedback'
        })
      }
    }
  }
})
