// component/tabBarPage/livePage/index.js
import storeData from '../storeData.js'
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    isShow: false,
    loaddingMessage: '正在加载...',
    tjList: [],
    ygList: [],
    hkList: [],
  },
  ready: function () {
    this.getData();
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getData: function () {
      let _self = this;
      if (storeData.liveData == null ||
        (storeData.liveData != null && storeData.liveData.code != 0)) {

        this.setData({
          isShow: true,
          loaddingMessage: '正在加载...'
        });
      } else {
        _self.setData({
          isShow: false,
          tjList: storeData.liveData.data.tjList,
          ygList: storeData.liveData.data.ygList,
          hkList: storeData.liveData.data.hkList,          
        });
      }
      storeData.requestLive(function (d) {
        if (d && d.code != 0) {
          _self.setData({
            isShow: true,
            loaddingMessage: '加载失败'
          });
        } else {
          _self.setData({
            isShow: false,
            tjList: d.data.tjList,
            ygList: d.data.ygList,
            hkList: d.data.hkList,
          });
        }
      });
    }
  }
})
