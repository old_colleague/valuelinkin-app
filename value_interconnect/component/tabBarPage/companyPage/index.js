// component/tabBarPage/companyPage/index.js
import storeData from '../storeData.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    isShow: false,
    loaddingMessage: '正在加载...',
    ssgs: [],
  },
  ready: function () {
    this.getData();
  },
  created	:function(){
    
  },
  /**
   * 组件的方法列表
   */
  methods: {
    openItem:function(e){
      let index = e.currentTarget.dataset.index;
      let obj = this.data.ssgs[index];
      this.setIsOpen(index,!obj.isShow,this.data.ssgs);
    },
    setIsOpen:function(index,isOpen,data){
      let _self = this;
      for (var i = 0; i < data.length; i++) {
        if (index != -1 && index == i){
          data[i].isShow = isOpen;
        } else if (index == -1){
          data[i].isShow = false;
        }
        for (var j = 0; j < data[i].stockList.length; j++) {
          if (index != -1 && index == i) {
            if (isOpen){
              data[i].stockList[j].isShow = isOpen;
            }else{
              if (j < 4) {
                data[i].stockList[j].isShow = true;
              } else {
                data[i].stockList[j].isShow = false;
              }
            }  
          } else if (index == -1){
            if (j < 4) {
              data[i].stockList[j].isShow = true;
            } else {
              data[i].stockList[j].isShow = false;
            }
          }
        }
      }
      _self.setData({
        isShow: false,
        ssgs: data
      });
    },
    getData:function(){
      let _self = this;
      if (storeData.ssgsData == null || 
        (storeData.ssgsData != null && storeData.ssgsData.code != 0)) {
          
        this.setData({
          isShow: true,
          loaddingMessage: '正在加载...'
        });
      }else{
        _self.setIsOpen(-1, false, storeData.ssgsData.data);
      }
      storeData.requestSsgs(function (d) {
        if (d && d.code != 0) {
          _self.setData({
            isShow: true,
            loaddingMessage: '加载失败'
          });
        } else {
          _self.setIsOpen(-1, false, d.data);
        }
      });      
    }
  }
})
