import { findIndexData, queryStockGroupPage, getZhiBoAllPage, getIndexData } from '../../service/homeService.js';
 
class StopreData {
  constructor() {
    this.isNetWork = false;
    this.homeData = null;
    this.ssgsData = null;
    this.liveData = null;
    this.viewpointData = null;
  }
  getHomeData(){
    return this.homeData;
  }
  getHomeDataRequest(callback){
    let _self = this;
    if (this.isNetWork) return;
    findIndexData(function(d){
      _self.isNetWork = false;
      _self.homeData = d;
      callback(d);
    });
  }
  requestSsgs(callback) {
    if (this.isNetWork) return;
    this.isNetWork = true;  
    var _self = this;
    queryStockGroupPage(function (d) {
      _self.isNetWork = false;
      _self.ssgsData = d;
      callback(d);
    });
  }
  requestLive(callback){
    if (this.isNetWork) return;
    this.isNetWork = true;
    var _self = this;
    getZhiBoAllPage(function(d){
      _self.isNetWork = false;
      _self.liveData = d;
      callback(d);
    });
  }
  requestViewpoint(callback){
    if (this.isNetWork) return;
    this.isNetWork = true;
    var _self = this;
    getIndexData(function (d) {
      _self.isNetWork = false;
      _self.viewpointData = d;
      callback(d);
    });
  }

}

module.exports = new StopreData();