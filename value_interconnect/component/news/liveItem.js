import { timestampFormat } from '../../utils/util.js';

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item: {
      type: Object,
      value: {},
    },
    type:{
      type:Number,
      value:0,
    }
  },
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    gotoDetail:function(){
      var type = this.data.item.nwsStatus == 2 || this.data.item.nwsStatus == 3 ? 2 : this.data.item.nwsStatus;
      wx.navigateTo({
        url: '/pages/live/liveDetail?nwsId=' + this.data.item.nwsId + "&type=" + type
      })
    }
  }
})
