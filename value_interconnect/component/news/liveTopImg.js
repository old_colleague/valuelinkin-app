// component/news/liveTopImg.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    items: {
      type: Array,
      value: [],
    },
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    itemChange:function(){
     
    },
    gotoDetial: function (e) {
      var item = this.data.items[e.currentTarget.dataset.index];
      var type = item.nwsStatus == 2 || item.nwsStatus == 3 ? 2 : item.nwsStatus;
      wx.navigateTo({
        url: '/pages/live/liveDetail?nwsId=' + item.nwsId + "&type=" + type
      })
    }
  }
})
