// component/news/viewpointConent.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item:{
      type: Object,
      value: {}
    },
    type:{
      type:Number,
      value:0 //0观点， 1精彩回顾
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
      
  },
  attached:function(){
  },

  /**
   * 组件的方法列表
   */
  methods: {
    gotoDetial:function(){
      var id =  this.data.item.opnId;
      if (this.data.type == 1){
        wx.navigateTo({
          url: '/pages/live/liveDetail?nwsId=' + id + "&type=2"
        })
        return;
      }
      wx.navigateTo({
        url: '/pages/viewpoint/viewpointDetail?opnId=' + id
      })
    }
  }
})
