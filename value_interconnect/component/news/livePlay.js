// component/news/livePlay.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item: {
      type: Object,
      value: {},
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    fullScreenFlag:false,
    PlayerCtx:null,
    dirc: "vertical",
    isStop:false,
    isShow:true,
    time: null,
    isRunFull:false,
  },
  ready: function () {
    this.PlayerCtx = wx.createLivePlayerContext("live_play", this);
    this.autoHidenPlay();
  },
  /**
   * 组件的方法列表
   */
  methods: {
    autoHidenPlay(){
      var _self = this;
      this.data.time = setTimeout(function(){
        _self.setData({
          isShow:false,
        });
      },3500);
    },
    statechange(e) {
      console.log('live-player code:', e.detail.code)
    },
    error(e) {
      console.error('live-player error:', e.detail.errMsg)
    },
    fullscreenchange(e){
      if (!e.detail.fullScreen){
        this.setData({
          fullScreenFlag: false,
          dirc: "vertical"
        });
      }
    },
    tapPlay(){
      clearTimeout(this.data.time);
      this.setData({
        isShow: !this.data.isShow,
      });
      this.autoHidenPlay();
    },
    play(){
      if (this.data.isStop){
        this.PlayerCtx.resume();
        if (this.data.isRunFull){
          this.data.isRunFull = false;
          var _self = this;
          _self.setData({
            dirc: this.data.dirc == "horizontal" ? "vertical" : "horizontal"
          });
          _self.setData({
            dirc: this.data.dirc == "horizontal" ? "vertical" :"horizontal"
          });
        }
      }else{
        this.PlayerCtx.pause();
      }
      
      this.setData({
        isStop:!this.data.isStop
      });
    },
    fullScreen: function () {
      if (this.data.isStop){
        this.data.isRunFull = true;
      }
      clearTimeout(this.data.time);
      var that = this;
      //全屏
      var vidoHeight = wx.getSystemInfoSync().windowHeight;
      var fullScreenFlag = that.data.fullScreenFlag;
      if (fullScreenFlag) {
        fullScreenFlag = false;
      } else {
        fullScreenFlag = true;
      }
      
      if (fullScreenFlag) {
        //全屏
        that.PlayerCtx.requestFullScreen({
          success: res => {
            that.setData({
              fullScreenFlag: fullScreenFlag
            });
            console.log('我要执行了');
          },
          fail: res => {
            console.log('fullscreen fail');
          }
        });
        this.setData({
          dirc:"horizontal",
          isShow: false
        });

      } else {
        //缩小
        that.PlayerCtx.exitFullScreen({
          success: res => {
            console.log('fullscreen success');
            that.setData({
              fullScreenFlag: fullScreenFlag
            });
          },
          fail: res => {
            console.log('exit fullscreen success');
          }
        });
        this.setData({
          dirc: "vertical",
          isShow: false
        });
      }

    },
  }
})
