import { timestampFormat, trim } from '../../utils/util.js';
import { getMyQuestionPage, saveQuestion, publicSuggestion } from '../../service/myService.js';

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    nwsId:{
      type:String,
      value:""
    },
    type:{
      type:Number,
      value:0 // 0直播提问，1用户反馈
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    inputValue:""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    bindKeyInput:function(e){
      this.data.inputValue = e.detail.value;
    },
    //发送问题
    sendProblem:function(){
      if (trim(this.data.inputValue).length == ""){
        wx.showToast({
          title: "请输入内容",
          icon: 'none',
          duration: 2000
        });
        return;
      }
      wx.showLoading({
        title: '请稍后...',
      })
      var _self = this;
      // 
      if (this.data.type == 1){
        publicSuggestion(this.data.inputValue,function(d){
          wx.hideLoading();
          if (d) {
            wx.showToast({
              title: "发送成功",
              icon: 'none',
              duration: 1000
            });
            _self.setData({
              inputValue: ""
            });
            _self.triggerEvent('refreshProblemList', 0);//调用父组件，通知刷新 改变。
          }
        });
      }else{
        saveQuestion(this.data.nwsId, this.data.inputValue, function (d) {
          wx.hideLoading();
          if (d) {
            wx.showToast({
              title: "提问成功",
              icon: 'none',
              duration: 1000
            });
            _self.setData({
              inputValue: ""
            });
            _self.triggerEvent('refreshProblemList', 0);//调用父组件，通知刷新 改变。
          }
        });
      }
    }
  }
})
